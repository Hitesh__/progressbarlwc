import {
    LightningElement,
    track
} from 'lwc';

export default class ProgressBar extends LightningElement {

    @track progress = 0;
    @track processStatus = 'In Progress';

    connectedCallback() {
        let interval = setInterval(() => {
            this.progress = this.progress + 5;
            this.processStatus = 'Processing => ' + this.progress + '/100';
            if (this.progress === 100) {
                clearInterval(interval);
                this.processStatus = 'Completed';
            }
        }, 300);

    }
}